import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BasketService } from '../basket/basket/basket.service';
import { IUser } from '../shared/models/user';
import { AccountService } from '../account/AccountService';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss',
})
export class NavBarComponent implements OnInit {
  cartItem: any = Observable<[any]>;
  total: number = 0;
  currentUser$?: Observable<IUser>;
  cart:any;
  profile: any;
 
  constructor(
    private basketService: BasketService,
    private accountService: AccountService
  ) {


    this.profile = JSON.parse(localStorage.getItem('profile') as string);
   
  }
  ngOnInit(): void {
 

    this.cartItem = this.basketService.productIdList;
    this.total = this.basketService.getTotalPrice();
    this.currentUser$=this.accountService.currentUser$;
  this.getCart()



  }

  getCart() {
    const carts = JSON.parse(localStorage.getItem('cartItem') as string);
    this.cart = carts;
  }
  logout(){
    this.accountService.logout();
  }
}
