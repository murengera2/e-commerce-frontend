import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrl: './checkout.component.scss'
})
export class CheckoutComponent implements OnInit {

  cartItem: any = [];

  constructor(){}

  ngOnInit(): void {
    this.getCart();
      
  }

getCart() {
    const cart = JSON.parse(localStorage.getItem('cartItem') as string);
    this.cartItem = cart;
  }

}
