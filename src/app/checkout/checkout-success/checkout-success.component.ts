import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders/orders.service';

@Component({
  selector: 'app-checkout-success',
  templateUrl: './checkout-success.component.html',
  styleUrl: './checkout-success.component.scss'
})
export class CheckoutSuccessComponent  implements OnInit{
  order:any;
  constructor(private orderService: OrdersService){}


  ngOnInit(): void {
    this.getOrders();
      
  }
  getOrders() {
    this.orderService.getOrdersForUser().subscribe(
      (response: any) => {
        this.order = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
