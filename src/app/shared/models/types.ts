export interface ProductType{
    id: string;
    name: string;
    is_active: boolean;
    is_deleted: boolean;
    created_at: string | null;
    modified_by: string; 
    created_by: string; 
  }
  