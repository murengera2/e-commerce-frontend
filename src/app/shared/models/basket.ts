export interface IBasket {
  id?: string;
  items?: IBasketItem[];
}

export interface IBasketItem {
  id?: string | null;
  productName?: string | null;
  price?: number | null;
  quantity?: number | null;
  pictureUrl?: string | null;
  brand?: string | null;
  type?: string | null;
  created_at?: string | null;
  modified_at?: string | null;
  created_by?: number | null;
}

export class Basket implements IBasket {
  id?: string;
  items?: IBasketItem[] = [];
}

export interface IBasketTotals {
  shipping: number;
  subtotal: number;
  total: number;
}
