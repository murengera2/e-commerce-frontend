export interface Order {
    id?: string; // UUIDs are represented as strings in TypeScript
    referenceCode?: string;
    products?: any[]; // An array to represent the JSONField set to default as a list
    subtotal?: number;
    fees?: number;
    total?: number;
    status?: 'NEW' | 'WAITING_PAYMENT_RESPONSE' | 'PAYING' | 'FINISHED' | 'ACTIVE' | 'ON_WAY' | 'DRIVER_ARRIVED' | 'DENIED' | 'CANCELLED';
    isPaid?: boolean;
    createdBy?: number; // Assuming User ID is numeric. Change to string if User ID is a UUID.
    createdAt?: Date;
    modifiedAt?: Date;
}
