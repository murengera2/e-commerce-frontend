export interface HttpPagedResponse<T> {
  pagination: {
    count: number;
    page_size: number;
    page: number;
    total_pages: number;
  };
  links: {
    next: string | null;
    previous: string | null;
  };
  results: T[];
}

