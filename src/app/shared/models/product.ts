 export interface IProduct {
    id: string; 
    name: string; 
    category: string; 
    brand: string; 
    description: string;
    price: number;
    pictureUrl: string; 
    is_available: boolean; 
    in_stock: number; 
    is_deleted: boolean; 
    created_by: string; 
    created_at: Date | null; 
    modified_at: Date; 
  }
  