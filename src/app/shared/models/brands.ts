export interface ProductBrand{
    id: string;
    name: string;
    is_active: boolean;
    is_deleted: boolean;
    created_at: string | null;
    modified_by: string; 
    created_by: string; 
  }
  

  export interface PaginatedBrandResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: Brand[];
  }
  
  export interface Brand {
    id: string;
    name: string;
    is_active: boolean;
    is_deleted: boolean;
    created_at: string;  
    modified_by: string;
  }
  