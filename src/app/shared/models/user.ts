export interface IUser{
    id: string;
  name: string;
  last_name: string;
  first_name: string;
  email: string;
  phone_number: string;
  national_id: string;
  is_deleted: boolean;
  is_active: boolean;
  password: string;
  username: string;
  token: string;
}