import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopModule } from '../shop/shop.module';
import { OrderToolsComponent } from './components/order-tools/order-tools.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {CdkStepperModule} from '@angular/cdk/stepper';
import { StepperComponent } from './components/stepper/stepper.component';
import { OrdersModule } from '../orders/orders.module';

@NgModule({
  declarations: [OrderToolsComponent, StepperComponent],
  imports: [CommonModule, ShopModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    CdkStepperModule,
    OrdersModule,
    
   
    
  ],
  exports: [ShopModule, OrderToolsComponent,ReactiveFormsModule,StepperComponent,OrdersModule,
    CdkStepperModule,
    NgbDropdownModule,
   
  ],
})
export class SharedModule {}
