import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IBasketTotals } from '../../models/basket';
import { BasketService } from '../../../basket/basket/basket.service';

@Component({
  selector: 'app-order-tools',
  templateUrl: './order-tools.component.html',
  styleUrl: './order-tools.component.scss',
})
export class OrderToolsComponent implements OnInit {
  totals?: any = Observable<IBasketTotals>;
  totalPrice?: any = [];

  constructor(private basketService: BasketService) {}
  ngOnInit(): void {
    this.basketService.cartTotal$?.subscribe((response) => {
      this.totals = response;
    });
  }
}
