import { Component, OnInit } from '@angular/core';
import { BasketService } from '../../../basket/basket/basket.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBasketTotals } from '../../models/basket';
import { Order } from '../../models/order';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrl: './stepper.component.scss',
})
export class StepperComponent implements OnInit {
  products: any = [];
  cartItem: any = [];
  cart: any;


  user = JSON.parse(localStorage.getItem('profile') as string);
  totals?: any = Observable<IBasketTotals>;
  order?: any = [];

  constructor(
    private api: BasketService,
    private router: Router,
    private httpClient: HttpClient,
    private basketService: BasketService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.basketService.cartTotal$?.subscribe((response) => {
      this.totals = response;
    });
    this.getCart();
  }

  getCart() {
    const cart = JSON.parse(localStorage.getItem('cartItem') as string);
    this.cartItem = cart;
  }

  placeOrder() {
    const path = 'OrderModel';

    this.cartItem.forEach((element: any) => {
      const product = {
        id: element.id,
        quantity: element.quantity,
        price: element.price,
        pictureUrl: element.pictureUrl,
        name: element.name,
      };

      this.products.push(product);

      this.order = {
        products: this.products,

        total: this.totals.total,
        subtotal: this.totals.subtotal,
        created_by: this.user.id,
      };
    });

    this.api.postData(path, this.order).subscribe(
      (response: any) => {
        console.log("response order",response)

        this.basketService.removeAllCart();
        this.toastr.show("Order Created Successfuly!")
        // window.location.reload();
        JSON.stringify(localStorage.setItem("orderid",response.id))

        this.router.navigate([`/orders/${response.id}`]);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
