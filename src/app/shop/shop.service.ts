import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpPagedResponse } from '../shared/models/pagination';
import { IProduct } from '../shared/models/product';
import {
  Brand,
  PaginatedBrandResponse,
  ProductBrand,
} from '../shared/models/brands';
import { ProductType } from '../shared/models/types';
import { map } from 'rxjs';
import { ShopParams } from '../shared/models/shopParams';

@Injectable({
  providedIn: 'root',
})
export class ShopService {
  baseUrl = 'https://ecommerceapi-3b8u.onrender.com/api/';

  constructor(private http: HttpClient) {}

  getProducts(shopParams: ShopParams) {
    let params = new HttpParams();
    if (shopParams.brandId) {
      params = params.append('brand', shopParams.brandId.toString());
    }
    if (shopParams.categoryId) {
      params = params.append('category', shopParams.categoryId.toString());
    }

    if (shopParams.order) {
      params = params.append('ordering', shopParams.order);
    }

    return this.http
      .get<HttpPagedResponse<IProduct>>(this.baseUrl + 'ProductModel', {
        observe: 'response',
        params,
      })
      .pipe(
        map((response) => {
          return response.body;
        })
      );
  }
  getProduct(id:string){
    return this.http.get<IProduct>(this.baseUrl+'ProductModel/'+id);
  }
  getBrand() {
    return this.http.get<Brand[]>(this.baseUrl + 'brand');
  }
  getCategory() {
    return this.http.get<ProductType[]>(this.baseUrl + 'category');
  }
}
