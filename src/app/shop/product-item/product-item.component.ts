import { HttpClient } from '@angular/common/http';
import { Component,OnInit,Input } from '@angular/core';
import { IProduct } from '../../shared/models/product';
import { BasketService } from '../../basket/basket/basket.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrl: './product-item.component.scss'
})
export class ProductItemComponent  implements OnInit{
  @Input() product?:IProduct|any;
  
  constructor( private http:HttpClient,private basketService:BasketService){

  }
  ngOnInit(): void {

  }
  addItemToBasket(){
   
    this.basketService.addToCart(this.product);

    window.location.reload()
    console.log( this.basketService.addToCart(this.product))

  }

}
