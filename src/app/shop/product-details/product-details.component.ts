import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../shared/models/product';
import { ShopService } from '../shop.service';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from 'xng-breadcrumb';
import { BasketService } from '../../basket/basket/basket.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.scss',
})
export class ProductDetailsComponent implements OnInit {
  product?: IProduct;
  productId?: any;
  quantity: number = 1;
  constructor(
    private shopServise: ShopService,
    private activateRoute: ActivatedRoute,
    private bsService: BreadcrumbService,
    private basketService: BasketService
  ) {}

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params) => {
      this.productId = params['id'];
    });
    this.loadProduct();
  }

  addItemToBasket() {
    this.loadProduct();
   console.log(this.basketService.addToCart(this.product)) 
  }

  incrementeQuantity() {
    this.quantity++;
  }
  decreamentQuantity() {
    if(this.quantity>1){
      this.quantity--;
    }
   
  }
  loadProduct() {
    this.shopServise.getProduct(this?.productId).subscribe(
      (product) => {
        this.product = product;
        console.log(this.product)
        this.bsService.set('@productDetails', product.name);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
