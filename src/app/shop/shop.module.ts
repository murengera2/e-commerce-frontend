import { NgModule,Pipe } from '@angular/core';
import { ShopComponent } from './shop.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ShopRoutingModule } from './shop-routing.module';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { loadingInterceptor } from '../core/interceptors/laoding.interceptors';
import { CarouselModule } from 'ngx-bootstrap/carousel';


@NgModule({
  declarations: [
    ShopComponent,
    ProductItemComponent,
    PaginatorComponent,
    
  ],
  imports: [
    ShopRoutingModule,
    NgbPaginationModule,
    RouterModule,
    CommonModule,
    CarouselModule
  
  ],
  exports:[
    CarouselModule
  ],
  providers:[ {provide:HTTP_INTERCEPTORS,useClass:loadingInterceptor,multi:true}]
 
})
export class ShopModule { }
