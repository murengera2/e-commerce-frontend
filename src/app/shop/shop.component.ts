import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ShopService } from './shop.service';
import { IProduct } from '../shared/models/product';
import { Brand, ProductBrand } from '../shared/models/brands';
import { ProductType } from '../shared/models/types';
import { ShopParams } from '../shared/models/shopParams';
import { delay } from 'rxjs';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrl: './shop.component.scss',
})
export class ShopComponent implements OnInit {
  products?: IProduct[];
  shopParams = new ShopParams();
  brands?: Brand[];
  categories?: ProductType[];
  @Input() showPagination = true;

  @Input() totalElementsSize?: number;
  @Input() currentPage: number = 1;
  @Input() pageSize: number = 20;
  @Input() sizes: number[] = [10, 20, 50];
  @Input() loadingItems = false;

  pagination = {
    pageSize: 20,
    pageNumber: 1,
    totalNumberOfElements: 0,
  };

  orderOptions = [
    {
      name: 'Alphabetical',
      value: 'name',
    },
    { name: 'Price:Low to High', value: 'price' },

    { name: 'Price:High to High', value: 'price' },
  ];

  constructor(private shopService: ShopService) {}
  ngOnInit(): void {
    this.getProducts();
    this.getBrands();
    this.getCategories();
  }

  paginationChange($event: any) {
    this.pagination = {
      pageNumber: $event.page,
      pageSize: $event.size,
      totalNumberOfElements: 0,
    };
    this.getProducts();
  }

  getProducts() {
    this.shopService.getProducts(this.shopParams).subscribe(
     
      (response: any) => {
      
       
        this.products = response.results;
        this.shopParams.pageNumber = response.count;
        this.shopParams.pageSize = response.page;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getBrands() {
    this.shopService.getBrand().subscribe(
     
      (response: any) => {
        
        this.brands = [{ id: 0, name: 'All' }, ...response?.results];
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getCategories() {
    this.shopService.getCategory().subscribe(
      (response: any) => {
        this.categories = [{ id: 0, name: 'All' }, ...response?.results];
      },
      (error) => {
        console.log(error);
      }
    );
  }
  onBrandSelected(brandId: string) {
    this.shopParams.brandId = brandId;
    this.getProducts();
  }

  onTypeSelected(typeId: string) {
    this.shopParams.categoryId = typeId;
    this.getProducts();
  }
  onOrderSelected(event: any) {
    this.shopParams.order = event?.target?.value;

    this.getProducts();
  }
}
