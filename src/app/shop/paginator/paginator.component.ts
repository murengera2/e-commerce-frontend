import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'fydmee-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnInit{
  
  @Input() totalElementsSize?: number;
  @Input() currentPage: number = 1;
  @Input() pageSize: number = 20;
  @Input() sizes: number[] = [10, 20, 50];
  

  @Output() paginationChange: EventEmitter<any> = new EventEmitter<any>();

  totalPages?: number;
  pages?: number[];

  ngOnInit(): void {
    this.totalPages = Math.ceil(this.totalElementsSize! / this.pageSize);
    this.pages = Array.from(Array(this.totalPages), (_,x) => x + 1);
  }

  pageSizeChange($event: any) {
    this.pageSize = parseInt($event.target.value);
    this.paginationChange.emit({
      size: parseInt($event.target.value),
      page: this.currentPage,
    });
  }

  pageNumberChanged($event: number) {
    this.currentPage = $event;
    this.paginationChange.emit({
      size: this.pageSize,
      page: $event,
    });
  }
}

