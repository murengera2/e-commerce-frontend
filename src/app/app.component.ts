import { Component, OnInit } from '@angular/core';
import { BasketService } from './basket/basket/basket.service';
import { IBasket, IBasketItem } from './shared/models/basket';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'client';



  constructor() {}

  ngOnInit(): void {
 
  }
}
