import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, isEmpty, map } from 'rxjs';
import {
  Basket,
  IBasket,
  IBasketItem,
  IBasketTotals,
} from '../../shared/models/basket';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IProduct } from '../../shared/models/product';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class BasketService {
  baseUrl = environment.apiUrl;
  public cartItemList: any = [];
  public productList = new BehaviorSubject<any>([]);
  public quanti:number=1;
  public productIdList: string[] = [];
  sessionStorage: any = window.sessionStorage;
  private cartTotalSource = new BehaviorSubject<IBasketTotals | null>(null);
  cartTotal$? = this.cartTotalSource.asObservable();
  product?:IProduct;
  productId?:any;

  api_root = 'http://127.0.0.1:8000/orders/';
   token :any;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };

  fhttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };
  
  

  constructor(private http: HttpClient, private toastr: ToastrService) {
    const items = JSON.parse(localStorage.getItem('cartItem') as string);

   
    if(JSON.parse(localStorage.getItem('profile')as string)!==null){
       this.token=JSON.parse(localStorage.getItem('profile')as string)['token'];

      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Token ${this.token}`,
        }),
      };
    

    }
  

    if (items != null && items != undefined) {
      this.cartItemList = items;
      this.cartItemList.forEach((element: any) => {
        this.productIdList.push(element.id);
      });
      this.productList.next(this.cartItemList);
      this.calculateTotals();
    }
  }
  addToCart(product: any, remove: boolean = false) {
    const index = this.productIdList.indexOf(product.id);

    if (index == -1) {
      product['quantity'] = 1;

      this.cartItemList.push(product);
      this.productIdList.push(product.id);
    } else {
      const prod = this.cartItemList[index];

      if (remove) {
        prod.quantity = prod.quantity - 1;

        if (prod.quantity == 0) {
          this.cartItemList.splice(index, 1);
          this.productIdList.splice(index, 1);
        }
      } else {
        if (prod.quantity < prod.in_stock) {
          prod.quantity = prod.quantity + 1;
          this.cartItemList[index] = prod;
          this.quanti=  prod.quantity;
          
        } else {
          this.toastr.error('You excessed the quantity in stock');
        }
      }
    }
    this.productList.next(this.cartItemList);

    localStorage.setItem('cartItem', JSON.stringify(this.cartItemList));
    // this.getTotalPrice();
    this.calculateTotals();
  }


  postData(path: string, data: []) {
    const url = `${this.api_root}${path}`;
    return this.http.post(url, data,this.httpOptions);
  }


  getTotalPrice() {
    let grandTotal = 0;
    this.cartItemList.forEach((element: any) => {
     
      grandTotal += element.price;
    });
    return grandTotal;
  }

  removeAllCart() {
    this.cartItemList = [];
    this.productIdList = [];
    this.productList.next(this.cartItemList);
    localStorage.removeItem('cartItem');
  }

  removeItem(position: number) {
    this.cartItemList.splice(position, 1);
    this.productIdList.splice(position, 1);
    this.productList.next(this.cartItemList);
    this.sessionStorage.setItem('cartItem', JSON.stringify(this.cartItemList));
    this.getTotalPrice();
  }

  incrementItemQuantity(item: any) {
 
    const cart = this.cartItemList;
  
    const foundItemIndex = cart.findIndex((x: any) => x.id === item.id);
   
    cart[foundItemIndex].quantity++;
   
    this.addToCart(cart);
  }

  decrementItemQuantity(item:any){
    const cart=this.cartItemList;
    const foundItemIndex=cart.findIndex((x:any)=>x.id===item.id);
    if(cart[foundItemIndex].quantity>1){
      cart[foundItemIndex].quantity--;
    }
    else{
      this.removeItem(cart)
    }


  }
  private calculateTotals() {
    const shipping = 0;
    const subtotal = this.cartItemList.reduce(
      (a: any, b: any) => b.price * b.quantity + a,
      0
    );
    const total = subtotal + shipping;
    this.cartTotalSource.next({
      shipping,
      total,
      subtotal,
    });
    
  }
}
