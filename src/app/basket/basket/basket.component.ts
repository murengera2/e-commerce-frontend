import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BasketService } from './basket.service';
import { ActivatedRoute } from '@angular/router';
import { ShopService } from '../../shop/shop.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrl: './basket.component.scss',
})
export class BasketComponent implements OnInit {
  cartItem: any = Observable<any>;
  quantity: number = 1;
  canAddQuantity: boolean = true;
  canRemoveQuantity: boolean = true;
  product: any;
  productId?: any;

  constructor(
    private basketService: BasketService,
    private activateRoute: ActivatedRoute,
    private shopServise: ShopService
  ) {}
  ngOnInit(): void {
    console.log('cartItem', this.cartItem);

    this.activateRoute.params.subscribe((params) => {
      this.productId = params['id'];
    });

    this.getCart();
  }

  getCart() {
    const cart = JSON.parse(localStorage.getItem('cartItem') as string);
    this.cartItem = cart;
    if (cart) {
      this.cartItem = cart;
      this.basketService.getTotalPrice();
    }
  }
  removeItem(position: number) {
    this.basketService.removeItem(position);
    window.location.reload();
  }

  incrementItem(item: any) {
    console.log(item);
    this.basketService.incrementItemQuantity(item);
  }
  decrementItem(item: number) {
    console.log(item);
    this.basketService.decrementItemQuantity(item);
  }

  changeQuantity(changeValue: number) {}
}
