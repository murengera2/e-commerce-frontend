import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from 'xng-breadcrumb';
import { OrdersService } from '../orders.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrl: './order-detail.component.scss',
})
export class OrderDetailComponent implements OnInit {
  orderId: any;
  order: any;
 

  constructor(
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.set('@OrderDetailed', '');
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.orderId = params.get('id');
    });

    this.orderService.getOrderDetailed(this.orderId).subscribe(
      (order: any) => {
        this.order = order;
       
        console.log(this.order)
      
        this.breadcrumbService.set(
          '@OrderDetailed',
          `Order# ${order.id} - ${order.status}`
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
