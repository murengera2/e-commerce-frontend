import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OrdersService } from '../orders.service';
import { BreadcrumbService } from 'xng-breadcrumb';

@Component({
  selector: 'app-order-done',
  templateUrl: './order-done.component.html',
  styleUrl: './order-done.component.scss',
})
export class OrderDoneComponent implements OnInit {
  isLoading = false;
  order_id?: any;
  order: any = {};
  profile = JSON.parse(localStorage.getItem('profile') as string);

  constructor(
    private activatedRoute: ActivatedRoute,
    private toast: ToastrService,
    private orderService: OrdersService,
    private breadcrumbService: BreadcrumbService
  ) {}

  ngOnInit(): void {
    this.getOrder();
  }

  getOrder() {
    this.order_id = localStorage.getItem('orderid');
    console.log(this.order_id);

    this.orderService.getOrderDetailed(this.order_id).subscribe(
      (order: any) => {
        console.log(order);
        this.order = order;

        console.log(this.order);

        this.breadcrumbService.set(
          '@OrderDetailed',
          `Order# ${order.id} - ${order.status}`
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  makePayment() {
    const path = 'transaction-actions/make-payment';
    const data = {
      order: this.order_id,
    };
    this.isLoading = true;
    this.orderService.postData(path, data).subscribe((response: any) => {
      console.log('response', response);

      this.isLoading = false;
      window.location.replace(response.pay_link);
    });
  }
}
