import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrderDoneComponent } from './order-done/order-done.component';



@NgModule({
  declarations: [
    OrderDetailComponent,
    OrderListComponent,
    OrderDoneComponent,
  
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule

  ]
})
export class OrdersModule { }
