import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  api_root = environment.apiUrl;
  BASE_URL='http://127.0.0.1:8000/orders/'
  BASE_URL_TRANSACTION='http://127.0.0.1:8000/'
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };

  fhttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };
  constructor(private http: HttpClient) {
    const token = JSON.parse(localStorage.getItem('profile')as string)['token'];
    if(JSON.parse(localStorage.getItem('profile')as string)!==null){

      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Token ${token}`,
        }),
      };
    }
  }
  


  get(path: string) {
    const url = `${this.BASE_URL}${path}`;
    return this.http.get(url, this.httpOptions);
  }


  getOrdersForUser() {
    return this.http.get('http://127.0.0.1:8000/orders/OrderModel',this.httpOptions);
  }
  getOrderDetailed(id: string) {
    return this.http.get('http://127.0.0.1:8000/orders/OrderModel/' + id,this.httpOptions);
  }

  postData(path: string, data: {}) {
    const url = `${this.BASE_URL_TRANSACTION}${path}`;
    return this.http.post(url, data,this.httpOptions);
  }

}
