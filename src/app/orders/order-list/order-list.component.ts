import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrl: './order-list.component.scss',
})
export class OrderListComponent implements OnInit {
  orders: any = [];
  profile:any;


  constructor(private orderService: OrdersService) {}
  ngOnInit(): void {
    this.getOrders();
 
  }


  getOrders() {
    this.profile = JSON.parse(localStorage.getItem('profile') as string);
    console.log("profile",this.profile.id)

    let path = `OrderModel?created_by=${this.profile.id}`;
    console.log(path)
    this.orderService.get(path).subscribe(
      (response: any) => {
        this.orders = response;
        console.log("orders",this.orders)
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
