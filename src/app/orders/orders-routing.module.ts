import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { CheckoutSuccessComponent } from '../checkout/checkout-success/checkout-success.component';
import { OrderDoneComponent } from './order-done/order-done.component';

const routes: Routes = [
  { path: '', component: OrderListComponent },
  {path:':orderId',component:OrderDoneComponent},
  {
    path: ':id',
    component: OrderDetailComponent,
    data: { breadcrumb: { alias: 'OrderDetailed' } },
  }

];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersRoutingModule {}
