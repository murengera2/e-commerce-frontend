import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { BehaviorSubject, map } from 'rxjs';
import { IUser } from '../shared/models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  baseUrl = environment;
  private currentUserSource = new BehaviorSubject<IUser | any>(null);
  currentUser$ = this.currentUserSource.asObservable();

  headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };

  fHeaders = {
    headers: new HttpHeaders({}),
  };

  constructor(private http: HttpClient, private router: Router) {
    const profile = JSON.parse(localStorage.getItem('profile') as string);

    if (profile) {
      this.headers = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Token ${profile.token}`,
        }),
      };

      this.fHeaders = {
        headers: new HttpHeaders({
          Authorization: `Token ${profile.token}`,
        }),
      };
    }
  }

  isAuthenticated(): boolean {
    const profile = JSON.parse(localStorage.getItem('profile') as string);

    if (profile !== undefined && profile !== null) {
      return true;
    }
    return false;
  }

  reInit() {
    const profile = JSON.parse(localStorage.getItem('profile') as string);

    if (profile) {
      this.headers = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Token ${profile.token}`,
        }),
      };

      this.fHeaders = {
        headers: new HttpHeaders({
          Authorization: `Token ${profile.token}`,
        }),
      };
    }
  }

  login(values: IUser) {
    return this.http
      .post(
        'http://127.0.0.1:8000/users/authentication/login',
        values,
        this.headers
      )
      .pipe(
        map((user: IUser | any) => {
          if (user) {
            localStorage.setItem('profile', JSON.stringify(user));
            this.reInit();
            this.currentUserSource.next(user);
          }
        })
      );
  }

  register(values: IUser) {
    return this.http
      .post('http://127.0.0.1:8000/users/authentication/create-account', values)
      .pipe(
        map((user: IUser | any) => {
          if (user) {
            localStorage.setItem('token', user.token);
          }
        })
      );
  }

  logout() {
    localStorage.removeItem('profile');
    this.currentUserSource.next(null);
    window.location.reload();
    this.router.navigateByUrl('/');
  }
}
