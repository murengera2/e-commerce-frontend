import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../AccountService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent implements OnInit {
  loginForm?: FormGroup;

  constructor(private accountService: AccountService, private router: Router) {}
  ngOnInit(): void {
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }
  onSubmit() {
    this.accountService.login(this.loginForm?.value).subscribe(
      (response) => {
      
        this.router.navigateByUrl('/checkout');
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
